#!/usr/bin/env python3

import os
import re
import urllib3

default_options = {
    # filename will override the destination filename check
    'filename': None,

    # anytype will skip the "is it a pdf?" check
    'anytype': False
}

headers = {
    'User-Agent': 'docbot - https://gitlab.com/treyzania/docbot'
}
http = urllib3.PoolManager(1, headers=headers)

class SiteHandler(object):
    def __init__(self):
        pass
    
    def matches(self, _url):
        return False

    def download(self, _url, _opts, _destdir):
        print('this doesn\'t work like that lol')

    def name(self):
        return None

CONTENT_DISP = 'Content-Disposition'

class DefaultSiteHandler(SiteHandler):
    def matches(self, _url):
        return True

    def download(self, url, opts, destdir):
        # Just make the request.
        r = http.request('GET', url)
        if r.status != 200:
            raise RuntimeError('response not OK: ' + r.status)

        # Verify it's a PDF, if we need to.
        ctype = r.headers['Content-Type']
        if ctype != 'application/pdf' and not opts['anytype']:
            raise RuntimeError('not a PDF, do you want "!anytype"?')

        # Figure out the destination name.
        name = opts['filename']
        if name is None:
            if CONTENT_DISP in r.headers:
                name = parse_disposition_filename(r.headers[CONTENT_DISP])
            else:
                name = filename_from_url(url)
                
        # If we can't figure it out on our own then ask for help.
        if name is None:
            raise RuntimeError('no suitable name found, specify with "!filename <name>"')

        destpath = find_acceptable_name(destdir, name)
        if destpath is None:
            raise RuntimeError('problem finding acceptable filename, try overriding')

        print('writing to path:', destpath)
        
        with open(destpath, 'wb') as f:
            f.write(r.data)

    def name(self):
        return 'default'

ARXIV_REGEX = re.compile(r'https?://arxiv\.org/(abs|pdf)/.*')

class ArxivSiteHandler(SiteHandler):
    def matches(self, url):
        return ARXIV_REGEX.match(url) is not None

    def download(self, url, opts, destdir):
        # In case they put an abstract link, convert it to the PDF link.
        realurl = url
        if '/abs/' in url:
            realurl = url.replace('/abs/', '/pdf/') + '.pdf'
            print('converted URL:', realurl)

        destname = filename_from_url(realurl)
        if opts['filename'] is not None:
            destname = opts['filename']

        # Now make the request and make sure it's ok.
        r = http.request('GET', realurl)
        if r.status != 200:
            raise RuntimeError('response not OK: ' + r.status)

        # Figure out destination path.
        destpath = find_acceptable_name(destdir, destname)

        print('writing to path:', destpath)

        with open(destpath, 'wb') as f:
            f.write(r.data)

    def name(self):
        return 'arxiv'

IACR_EPRINT_REGEX = re.compile(r'https?://eprint\.iacr\.org/[0-9]{4,}/[0-9]+(\.pdf)?')

class IacrEprintSiteHandler(SiteHandler):
    def matches(self, url):
        return IACR_EPRINT_REGEX.match(url) is not None

    def download(self, url, opts, destdir):
        # In case they put an abstract link, convert it to the PDF link.
        realurl = url
        if not url.endswith('.pdf'):
            realurl = url + '.pdf'

        parts = realurl.split('/')
        destname = 'iacr-' + parts[len(parts) - 2] + '-' + parts[len(parts) - 1]

        # Now make the request and make sure it's ok.
        r = http.request('GET', realurl)
        if r.status != 200:
            raise RuntimeError('response not OK: ' + r.status)

        # Figure out destination path.
        destpath = find_acceptable_name(destdir, destname)

        print('writing to path:', destpath)

        with open(destpath, 'wb') as f:
            f.write(r.data)

    def name(self):
        return 'iacr-eprint'

DISP_SPLIT = re.compile('; *')
def parse_disposition_filename(disp):
    parts = DISP_SPLIT.split(disp)
    for p in parts:
        if p.startswith('filename'):
            fnparts = p.split('=')
            return fnparts[1].replace('=', ' ').strip()
    return None

def filename_from_url(url):
    # We don't want fancy query pages, so ignore this.
    if '?' in url:
        return None

    # Just split it up by the path components.
    parts = url.split('/')
    last = parts[len(parts) - 1]
    if last.endswith('.pdf'):
        return last
    return prefix + last + '.pdf'

def find_acceptable_name(d, name):
    easycase = os.path.join(d, name)
    if not os.path.exists(easycase):
        return name

    for i in range(1000):
        attempt = filename_append(name, '-' + str(i + 1))
        if not os.path.exists(os.path.join(d, attempt)):
            return attempt

    return None

def filename_append(name, suffix):
    root, ext = os.path.splitext(name)
    return root + suffix + ext
    
# class IacrSiteHandler(SiteHandler)
# etc...

HANDLERS = [
    ArxivSiteHandler(),
    IacrEprintSiteHandler(),
    DefaultSiteHandler()
]

def find_handler_for_url(url):
    for h in HANDLERS:
        if h.matches(url):
            return h
    # should never get here
    return None

def _main():
    import sys
    url = None
    opts = default_options.copy()
    at = 1
    while at < len(sys.argv):
        arg = sys.argv[at]
        if arg.startswith('--'):
            optarg = arg[2:]
            optname = optarg
            if '=' in optarg:
                parts = optarg.split('=', 2)
                optname = parts[0]
                val = parts[1]
                at += 1
                if val == 'true' or val == 't':
                    opts[optname] = True
                elif val == 'false' or val == 'f':
                    opts[optname] = False
                else:
                    opts[optname] = val
                continue
            # If not done with '='.
            if len(sys.argv) >= at:
                n = sys.argv[at + 1]
                nl = n.lower()
                if nl == 'true' or nl == 't':
                    opts[optname] = True
                    at += 2
                elif nl == 'false' or nl == 'f':
                    opts[optname] = False
                    at += 2
                else:
                    opts[optname] = nl
                    at += 1
        elif arg.startswith('-') and not arg.startswith('--'):
            opt = arg[1:]
            opts[opt] = True
            at += 1
        else:
            url = arg
            at += 1

    h = find_handler_for_url(url)
    print('using handler:', h.name())
    h.download(url, opts, '.')

if __name__ == '__main__':
    _main()
